/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_SIMO_VARIABLE_DELAY_LINE
#define IW_ITA_SIMO_VARIABLE_DELAY_LINE

#include <ITACriticalSection.h>
#include <ITADSPDefinitions.h>
#include <ITADataLog.h>
#include <ITASIMOVariableDelayLineBase.h>
#include <ITAStopWatch.h>
#include <atomic>
#include <map>

//! Vorw�rtsdeklarationen
class ITASampleBuffer;
class ITASampleFrame;
class IITASampleInterpolationRoutine;

//! Single-input multiple-output block-based variable delay line
/**
 * Diese Klasse realisiert Verz�gerungsglieder (variable delay-lines, VDLs)
 * mit frei einstellbarer und zur Laufzeit ver�nderbarer Verz�gerung.
 * Hierbei wird nur ein Kanal betrachtet.
 *
 * Die maximale Verz�gerung wird durch den internen Pufferspeicher voralloziert.
 * Da das Reservieren von Speicherplatz zur Laufzeit teuer sein kann, ist es ratsam,
 * eine grobe Sch�tzung vor Ausf�hrung einer Szene zu vollziehen und dem entsprechend
 * eine Verz�gerung zu setzen.
 *
 * Berechnungsvorschrift: Samples = ( Distance / SpeedOfSound ) * SampleRate
 *
 * Beispiel:
 * - Raumakustik, Ausbreitungspfade maximal rund 100 m: 13000 = 13e3 Samples
 * - Flugl�rm, Ausbreitungspfade bis zu 10 km: 1300000 = 13e5 Samples
 *
 * Mittels der Methode ReserveMaximumDelaySamples() oder
 * ReserveMaximumDelayTime() kann dieser Speicher den
 * ben�tigten Verz�gerungen zur Laufzeit angepasst werden.
 *
 * Dieses Modul erzeugt nur dann Latenz, wenn f�r den aktuellen Switching-Algorithmus
 * nicht gen�gend St�tzstellen zur Verf�gung stehen. Diese Latenz tritt nur dann auf,
 * wenn die Gesamtverz�gerung der VDL unter die Latenz der Interpolationsroutine
 * f�llt. Die VDL Implementierung erzwingt dann diese Latenz, um auf weitere St�tzwerte
 * zu warten. Anders interpretiert funktioniert die Verz�gerung durch die VDL nur bis
 * zu einem Minimalabstand, welcher durch die Interpolationsroutine begrenzt wird. Unterhalb
 * dieser Grenze kommt es zu keiner zeitlich korrekten Wiedergabe der Samples.
 *
 * Beispiel:
 * - lineare Interpolation: 1 Sample Latenz, d.h. VDL Verz�gerung 0 = t < 1 Sample => 1 - t = 1 Sample Latenz
 * - sinc-Interpolation: z.B. 12 Sample Latenz, d.h. VDL Verz�gerung t < 12 Sample => 12 - t Samples Latenz
 * *
 * TODO: Doku, Synchronit�t
 * - W�chst automatisch mit Setzen der Verz�gerung
 */

class ITA_DSP_API CITASIMOVariableDelayLine : public CITASIMOVariableDelayLineBase
{
public:
	//! Konstruktor der variablen Verz�gerungsleitung
	/**
	 * \param dSamplerate				Abtastrate [Hz]
	 * \param iBlocklength				Streaming-Blockl�nge [Anzahl Samples]
	 * \param fReservedMaxDelaySamples	Initiale maximale Verz�gerung [Anzahl Samples]
	 * \param iAlgorithm				Algorithmus (siehe #ITABase::InterpolationFunctions)
	 */
	CITASIMOVariableDelayLine( const double dSamplerate, const int iBlocklength, const float fReservedMaxDelaySamples, const int iAlgorithm );

	//! Destruktor der variablen Verz�gerungsleitung
	~CITASIMOVariableDelayLine( );

	//! Verfahren zur �nderung der Verz�gerung zur�ckgeben
	/**
	 * Gibt das momentan benutzte Verfahren zur Umsetzung der Verz�gerungs�nderung zur�ck
	 *
	 * \return Eine Nummer aus der Auflistung #SwitchingAlgorithm
	 *
	 */
	int GetAlgorithm( ) const;

	//! Verfahren zur �nderung der Verz�gerung setzen
	/**
	 * Setzt das momentan zu benutzende Verfahren zur Umsetzung der Verz�gerungs�nderung
	 *
	 * \param iAlgorithm Eine Nummer aus der Auflistung #SwitchingAlgorithm
	 *
	 */
	void SetAlgorithm( const int iAlgorithm );

	//! Minimal m�gliche Verz�gerung in Samples zur�ckgeben
	int GetMinimumDelaySamples( ) const;

	//! Minimal m�gliche Verz�gerung in Sekunden zur�ckgeben
	float GetMinimumDelayTime( ) const;

	//! Maximal Verz�gerung zur�ckgeben [Sekunden]
	/**
	 * Maximale m�gliche Verz�gerung auf dem momentan
	 * reservierten Pufferspeicher in Sekunden zur�ckgeben
	 *
	 * Siehe auch: CITAVariableDelayLine::ReserveMaximumDelaySamples(), CITAVariableDelayLine::SetDelaySamples()
	 */
	float GetReservedMaximumDelayTime( ) const;

	//! Pufferspeicher reservieren f�r die angegebene maximale Verz�gerung [Sekunden]
	/**
	 * Wie ReserveMaximumDelaySamples(), nur f�r Zeit in Sekunden
	 */
	void ReserveMaximumDelayTime( const float fMaxDelaySecs );

	//! Overall current latency of a cursor in seconds
	/**
	 * \return Delay and fractional delay in seconds
	 *
	 * \note Raises ITAException if cursor not valid
	 */
	float GetCurrentDelayTime( const int iCursorID ) const;

	//! Overall new latency of a cursor in seconds
	/**
	 * Gibt die neu eingestellte (und m�glicherweise noch nicht �bernommene) Gesamtverz�gerung der VDL als Zusammensetzung der Ganzzahl und des Sub-Sample zur�ck
	 *
	 * \note Raises ITAException if cursor not valid
	 */
	float GetNewDelayTime( const int iCursorID ) const;

	//! Verz�gerung setzen [Sekunden]
	/**
	 * Wie SetDelaySamples(), aber f�r Zeit in Sekunden.
	 */
	void SetDelayTime( const int iID, const float fDelaySecs );

	//! Pushes a block of samples at front of delay line
	/**
	 * Writes a block of samples to the input of the VDL.
	 * Does not increment block, has to be done manually (i.e. after read)
	 *
	 * @note block-oriented VDL usage is usually a three-step process: write-read-increment.
	 *
	 * @param[in] psbInput Buffer source for incoming samples
	 */
	void WriteBlock( const ITASampleBuffer* psbInput );

	//! Reads a block of samples for a read cursor and switches to new delay
	/**
	 * @note Incremet block processing after all cursors have benn processed / read.
	 *
	 * @param[in] iID Cursor identifier
	 * @param[out] psbOutput Buffer target for processed samples (must be initialized)
	 */
	void ReadBlock( const int iID, ITASampleBuffer* psbOutput );

	//! Increments processing to next block
	/**
	 * @note Make sure that all cursors have been processed, i.e. have read a block from
	 *       the delay line. Otherwise, dropouts occur.
	 */
	void Increment( );

	//! Reads a block of samples at all cursors (switches to new delay)
	/**
	 * The order of the channels in the sample frame will be linear over the
	 * internal cursor list, see GetCursorIDs()
	 *
	 * @param[in] psfOutput Initialized sample frame
	 *
	 */
	void ReadBlockAndIncrement( ITASampleFrame* psfOutput );

private:
	double m_dSampleRate; //!< Audio-Abtastrate
	int m_iBlockLength;   //!< Audio-Blockgr��e

	int m_iMaxDelay;           //!< Maximal einstellbare Verz�gerung (h�ngt von Puffergr��e ab)
	int m_iSwitchingAlgorithm; //!< Eingestellter Algorithmus zum Umschalten der Verz�gerung

	int m_iFadeLength; //!< �berblendl�nge f�r das Umschaltverfahren mittels Kreuzblende (Minimum von Blockl�nge oder 32 Samples)

	bool m_bBenchmark;
	ITAStopWatch m_swProcess; //!< StopWatch zur �berwachung der Berechnungsschleife

	IITASampleInterpolationRoutine* m_pInterpolationRoutine; //!< Zeiger auf Interpolationsroutine
};


#endif // IW_ITA_SIMO_VARIABLE_DELAY_LINE
