/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_VARIABLE_DELAY_LINE
#define IW_ITA_VARIABLE_DELAY_LINE

#include <ITACriticalSection.h>
#include <ITADSPDefinitions.h>
#include <ITADataLog.h>
#include <ITAStopWatch.h>
#include <atomic>

//! Vorw�rtsdeklarationen
class ITASampleBuffer;
class IITASampleInterpolationRoutine;

//! Daten-Logger der VDL aktivieren
#define ITA_DSP_VDL_DATA_LOG 0

//! Klasse f�r variable Verz�gerungsglieder (variable delay-lines, VDLs)
/**
 * Diese Klasse realisiert Verz�gerungsglieder (variable delay-lines, VDLs)
 * mit frei einstellbarer und zur Laufzeit ver�nderbarer Verz�gerung.
 * Hierbei wird nur ein Kanal betrachtet.
 *
 * Die maximale Verz�gerung wird durch den internen Pufferspeicher voralloziert.
 * Da das Reservieren von Speicherplatz zur Laufzeit teuer sein kann, ist es ratsam,
 * eine grobe Sch�tzung vor Ausf�hrung einer Szene zu vollziehen und dem entsprechend
 * eine Verz�gerung zu setzen.
 *
 * Berechnungsvorschrift: Samples = ( Distance / SpeedOfSound ) * SampleRate
 *
 * Beispiel:
 * - Raumakustik, Ausbreitungspfade maximal rund 100 m: 13000 = 13e3 Samples
 * - Flugl�rm, Ausbreitungspfade bis zu 10 km: 1300000 = 13e5 Samples
 *
 * Mittels der Methode ReserveMaximumDelaySamples() oder
 * ReserveMaximumDelayTime() kann dieser Speicher den
 * ben�tigten Verz�gerungen zur Laufzeit angepasst werden.
 *
 * Dieses Modul erzeugt nur dann Latenz, wenn f�r den aktuellen Switching-Algorithmus
 * nicht gen�gend St�tzstellen zur Verf�gung stehen. Diese Latenz tritt nur dann auf,
 * wenn die Gesamtverz�gerung der VDL unter die Latenz der Interpolationsroutine
 * f�llt. Die VDL Implementierung erzwingt dann diese Latenz, um auf weitere St�tzwerte
 * zu warten. Anders interpretiert funktioniert die Verz�gerung durch die VDL nur bis
 * zu einem Minimalabstand, welcher durch die Interpolationsroutine begrenzt wird. Unterhalb
 * dieser Grenze kommt es zu keiner zeitlich korrekten Wiedergabe der Samples.
 *
 * Beispiel:
 * - lineare Interpolation: 1 Sample Latenz, d.h. VDL Verz�gerung 0 = t < 1 Sample => 1 - t = 1 Sample Latenz
 * - sinc-Interpolation: z.B. 12 Sample Latenz, d.h. VDL Verz�gerung t < 12 Sample => 12 - t Samples Latenz
 *
 * TODO: Doku, Synchronit�t
 * - W�chst automatisch mit Setzen der Verz�gerung
 */
class ITA_DSP_API CITAVariableDelayLine
{
public:
	//! Umsetzung der Verz�gerungs�nderung
	/**
	 * Auflistung der Algorithmen, die zur Umsetzung einer Verz�gerungs�nderung
	 * zur Verf�gung stehen.
	 */
	enum SwitchingAlgorithm
	{
		SWITCH = 0,                  //!< Hartes umschalten
		CROSSFADE,                   //!< �berblenden im Zeitbereich mittels Kreuzblende (Kosinus-Quadrat)
		LINEAR_INTERPOLATION,        //!< Stauchen und Strecken im Zeitbereich durch lineare Interpolation (Polynominterpolation der Ordnung 1)
		WINDOWED_SINC_INTERPOLATION, //!< Stauchen und Strecken im Zeitbereich durch Interpolation mittels gefensterter si-Funktion
		CUBIC_SPLINE_INTERPOLATION,  //!< Stauchen und Strecken im Zeitbereich durch kubische Spline-Interpolation
	};

	//! Konstruktor der variablen Verz�gerungsleitung
	/**
	 * @param[in] dSampleRate				Abtastrate [Hz]
	 * \param iBlocklength				Streaming-Blockl�nge [Anzahl Samples]
	 * \param fReservedMaxDelaySamples	Initiale maximale Verz�gerung [Anzahl Samples]
	 * \param iAlgorithm				Algorithmus (siehe #SwitchingAlgorithm)
	 */
	CITAVariableDelayLine( const double dSampleRate, const int iBlocklength, const float fReservedMaxDelaySamples,
	                       const int iAlgorithm = CITAVariableDelayLine::CUBIC_SPLINE_INTERPOLATION );

	//! Destruktor der variablen Verz�gerungsleitung
	~CITAVariableDelayLine( );

	//! Verfahren zur �nderung der Verz�gerung zur�ckgeben
	/**
	 * Gibt das momentan benutzte Verfahren zur Umsetzung der Verz�gerungs�nderung zur�ck
	 *
	 * \return Eine Nummer aus der Auflistung #SwitchingAlgorithm
	 *
	 */
	int GetAlgorithm( ) const;

	//! Verfahren zur �nderung der Verz�gerung setzen
	/**
	 * Setzt das momentan zu benutzende Verfahren zur Umsetzung der Verz�gerungs�nderung
	 *
	 * \param iAlgorithm Eine Nummer aus der Auflistung #SwitchingAlgorithm
	 *
	 */
	void SetAlgorithm( int iAlgorithm );

	//! Minimal m�gliche Verz�gerung in Samples zur�ckgeben
	int GetMinimumDelaySamples( ) const;

	//! Minimal m�gliche Verz�gerung in Sekunden zur�ckgeben
	float GetMinimumDelayTime( ) const;

	//! Maximal Verz�gerung zur�ckgeben [Samples]
	/**
	 * Maximale m�gliche Verz�gerung auf dem momentan
	 *	reservierten Pufferspeicher in Samples zur�ckgeben
	 */
	float GetReservedMaximumDelaySamples( ) const;

	//! Maximal Verz�gerung zur�ckgeben [Sekunden]
	/**
	 * Maximale m�gliche Verz�gerung auf dem momentan
	 * reservierten Pufferspeicher in Sekunden zur�ckgeben
	 *
	 * Siehe auch: CITAVariableDelayLine::ReserveMaximumDelaySamples(), CITAVariableDelayLine::SetDelaySamples()
	 */
	float GetReservedMaximumDelayTime( ) const;

	//! Pufferspeicher reservieren f�r die angegebene maximale Verz�gerung [Samples]
	/**
	 * \note Die vorhandenen Daten bleiben erhalten
	 * \note Nicht vor parallelem Einstieg sicher
	 */
	void ReserveMaximumDelaySamples( float fMaxDelaySamples );

	//! Pufferspeicher reservieren f�r die angegebene maximale Verz�gerung [Sekunden]
	/**
	 * Wie ReserveMaximumDelaySamples(), nur f�r Zeit in Sekunden
	 */
	void ReserveMaximumDelayTime( float fMaxDelaySecs );

	//! Sub-Sample-Verz�gerung aktiviert
	/**
	 * \return Gibt zur�ck, ob Sub-Sample-Verz�gerungen aktiviert (true) oder deaktiviert (false) ist
	 */
	bool GetFractionalDelaysEnabled( ) const;

	//! Sub-Sample-Verz�gerung setzen
	/**
	 * \param bEnabled Aktiviert (true) oder deaktiviert (false) Sub-Sample-Verz�gerungen
	 */
	void SetFractionalDelaysEnabled( bool bEnabled );

	//! Gesamtverz�gerung zur�ckgeben [Samples]
	/**
	 * Gibt die Gesamtverz�gerung der VDL als Zusammensetzung der Ganzzahl und des Sub-Sample zur�ck
	 */
	float GetDelaySamples( ) const;

	//! Gesamtverz�gerung zur�ckgeben
	/**
	 * Gibt die Gesamtverz�gerung der VDL als Zusammensetzung der Ganzzahl und des Sub-Sample zur�ck
	 */

	float GetDelayTime( ) const;

	//! Gesamtverz�gerung zur�ckgeben [Zeit]
	/**
	 * Gibt die neu eingestellte (und m�glicherweise noch nicht �bernommene) Gesamtverz�gerung der VDL als Zusammensetzung der Ganzzahl und des Sub-Sample zur�ck
	 */
	float GetNewDelayTime( ) const;

	//! Gesamtverz�gerung zur�ckgeben [Samples]
	/**
	 * Gibt die Gesamtverz�gerung der VDL als Ganzzahl und als Sub-Sample zur�ck
	 *
	 * \return iIntegerDelay Ganzzahlwert der Verz�gerung (kleiner oder gleich der Gesamtverz�gerung)
	 * \return fFractionalDelay Bruch der Sub-Sample-Verz�gerung aus dem Wertebereich [0, 1)
	 *
	 */
	float GetDelaySamples( int& iIntegerDelay, float& fFractionalDelay ) const;

	//! Verz�gerung setzen [Samples]
	/**
	 * Setzt die Verz�gerung der VDL. Die Verz�gerungsanpassung wird
	 * sofort auf den aktuellen Leseblock angewendet.
	 *
	 * \note	Vergr��ert gegebenenfalls den internen Puffer auf das Doppelte der aktuellen Gr��e.
	 *			Dies kann unter Umst�nden zu einem Blockausfall f�hren, da die Operation teuer ist.
	 *			Es empfiehlt sich bereits bei der Initialisierung f�r ausreichend Speicher zu sorgen,
	 *			siehe ReserveMaximumDelaySamples().
	 *
	 * \note	Die Funktion darf nicht parallel betreten werden (non-reentrant)
	 */
	void SetDelaySamples( float fDelaySamples );

	//! Verz�gerung setzen [Sekunden]
	/**
	 * Wie SetDelaySamples(), aber f�r Zeit in Sekunden.
	 */
	void SetDelayTime( float fDelaySecs );

	//! L�scht alle internen gespeicherten Samples und setzt die Distanz auf 0
	void Clear( );

	//! Daten verarbeiten
	/**
	 * Diese Funktion wird immer dann aufgerufen, wenn ein neuer Block f�r die Audiohardware
	 * verarbeitet werden soll (1 Block eingeben, 1 Block entnehmen).
	 *
	 * \param psbInput Eingabepuffer (Block) der VDL
	 * \param psbOutput Ausgabepuffer (Block) der VDL
	 *
	 */
	void Process( const ITASampleBuffer* psbInput, ITASampleBuffer* psbOutput );

private:
	double m_dSampleRate;            //!< Audio-Abtastrate
	int m_iBlockLength;              //!< Audio-Blockgr��e
	int m_iVDLBufferSize;            //!< Gr��e des Puffers zum Speichern verz�gerter Samples
	ITASampleBuffer* m_psbVDLBuffer; //!< Puffer zum Speichern verz�gerter Samples (variable Gr��e, mindestens 2xBlockl�nge)
	ITASampleBuffer* m_psbTemp;      //!< Tempor�rer Puffer zum Arbeiten mit Samples (Gr��e: 2xBlockl�nge) (das k�nnte evtl. knapp sein)
	ITACriticalSection m_csBuffer;   //!< Zugriff auf Puffer sch�tzen

	int m_iWriteCursor;                 //!< Der Schreibzeiger ist immer Vielfaches der Blockl�nge
	int m_iMaxDelay;                    //!< Maximal einstellbare Verz�gerung (h�ngt von Puffergr��e ab)
	int m_iSwitchingAlgorithm;          //!< Eingestellter Algorithmus zum Umschalten der Verz�gerung
	std::atomic<float> m_fCurrentDelay; //!< Aktuelle Verz�gerung in Samples
	std::atomic<float> m_fNewDelay;     //!< Neue Verz�gerung in Samples
	bool m_bFracDelays;                 //!< Fractional Delay Filterung an/aus

	int m_iFadeLength; //!< �berblendl�nge f�r das Umschaltverfahren mittels Kreuzblende (Minimum von Blockl�nge oder 32 Samples)

	bool m_bStarted; //!< Statusvariable zur Initialisierung

	ITAStopWatch m_swBufferSizeInc; //!< StopWatch zur �berwachung von Speicherallozierungszeiten
	ITAStopWatch m_swProcess;       //!< StopWatch zur �berwachung der Berechnungsschleife
	int m_iNumberOfDropouts;        //!< Z�hlt die Anzahl durch die VDL verursachten Ausf�lle

	IITASampleInterpolationRoutine* m_pInterpolationRoutine; //!< Zeiger auf Interpolationsroutine

#if( ITA_DSP_VDL_DATA_LOG == 1 )

	//! Implementierungsklasse f�r Logger-Datum
	class VDLLogData : ITALogDataBase
	{
	public:
		static std::ostream& outputDesc( std::ostream& os );
		std::ostream& outputData( std::ostream& os ) const;

		float fCurrentDelay;
		float fNewDelay;
		float fResamplingFactor;
		int iTargetBlockSize;
		float fProcessingTime; //!< Zeit der Process-Routine in Millisekunden
	};

	ITABufferedDataLogger<VDLLogData> m_oDataLog; //!< Logger Datum f�r VDL spezifische Prozess-Information
#endif
};

#endif // IW_ITA_VARIABLE_DELAY_LINE
