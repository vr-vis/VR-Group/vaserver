/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_CLOCK
#define INCLUDE_WATCHER_ITA_CLOCK

#include <ITABaseDefinitions.h>
#include <string>

/**
 * Diese abstrakte Klasse definiert die Schnittstelle f�r Zeitgeber
 * welche zur (hochgenauen) Zeitmessung eingesetzt werden k�nnen.
 * Anwendung des Zeitgebers ist das (hochgenaue) Messen von Zeitintervallen.
 * Ein Zeitgeber gibt die Zeit in Sekunden aus. Diese Zeit hat nicht
 * zun�chst keine Bezugszeit. Die Semantik des Zahlenwertes (Sekunden)
 * ist im Allgemeinen nicht bekannt. Konkrete Realisierungen
 * k�nnen jedoch Semantik vorgeben (z.B. Wallclock-Time oder
 * vergangene Sekunden nach Systemstart).
 *
 * Intern arbeiten Zeitgeber meist mit Zeit-Z�hlern (vgl. Performance Counter,
 * TSC-Register), welche Zeitschritte als Ganzzahlen repr�sentieren.
 * Jeder Zeitgeber hat eine Aufl�sung  (kleinstes noch messbares Zeitintervall)
 * und damit auch eine Frequenz [Hertz] (Z�hler-Schritte pro Sekunde).
 * WICHTIG: Entwurfsvorgabe ist, das jeder Zeitgeber �ber seine gesamte
 * Lebenszeit eine konstante Aufl�sung und Frequenz hat.
 *
 * Um die Benutzung der Zeitgeber m�glichst einfach zu gestalten und eine
 * einheitliche Ma�einheit vorzugeben, werden Zeiten in Sekunden gemessen.
 *
 * Intern verwaltet die Klasse einen Standard-Zeitgebers f�r die jeweilige
 * Zielplatform (implementiert als Singleton). Dieser kann mit der Methode
 * getDefaultClock() bezogen werden.
 */

class ITA_BASE_API ITAClock
{
public:
	virtual ~ITAClock( ) { };

	//! Standard Zeitgeber zur�ckgeben
	static ITAClock* getDefaultClock( );

	//! Name zur�ckgeben
	/**
	 * Gibt den Namen des Zeitmessers zur�ck (z.B. Windows Performance Counters, Posix Real-time Clock)
	 */
	virtual std::string getName( ) const = 0;

	//! Aufl�sung zur�ckgeben
	/**
	 * Gibt die Aufl�sung, das hei�t das kleinste noch messbare Zeitintervall,
	 * in Sekunden zur�ck. In der Interpretation von Zeitz�hlern, entspricht die
	 * Aufl�sung der Periodendauer eines Z�hlerschrittes (timerticks).
	 */
	virtual double getResolution( ) const = 0;

	//! Frequenz zur�ckgeben
	/**
	 * Gibt die Frequenz, den Kehrwert der Aufl�sung, zur�ck in Hertz zur�ck.
	 * In der Interpretation von Zeitz�hlern, entspricht die Frequenz der
	 * Anzahl der Z�hlerschritte (timerticks) pro Sekunde.
	 */
	virtual double getFrequency( ) const = 0;

	//! Aktuelle Zeit zur�ckgeben
	/**
	 * Gibt die aktuelle Zeit des Zeitgebers in Sekunden zur�ck.
	 */
	virtual double getTime( ) = 0;
};

#endif // INCLUDE_WATCHER_ITA_CLOCK
