/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_FADE
#define INCLUDE_WATCHER_ITA_FADE

// ITABase
#include <ITABaseDefinitions.h>

//! Signal blenden
/**
 * Multipliziert alle Werte im Array pfData mit den entsprechenden Blendkoeffizienten.
 * Diese werden anhand der Parameter definiert.
 *
 * \param pfData		Array mit den zu blendenden Werten
 * \param iFadeLength	L�nge der Blendung (darf L�nge von pfData nicht �berschreiten)
 * @param iFadingSign  Fade in or out
 * @param iFadingFunction Fading function
 */
void ITA_BASE_API Fade( float* pfData, int iFadeLength, int iFadingSign, int iFadingFunction );

//! Signal blenden (erweitert)
/**
 * Erweiterte Blend-Funktion, welche auch die Durchf�hrung einer Blendung in mehreren
 * Schritten erlaubt. Die Position in der �berblendung kann frei gew�hlt werden.
 *
 * \param pfData		Array mit den zu blendenden Werten
 * \param iFadeLength	L�nge der Blendung (darf hier die L�nge die  pfData nicht �berschreiten)
 * @param iFadingSign  Fade in or out
 * @param iFadingFunction Fading function
 * \param iOffset		Start-Position in der gesamten Blendung
 *                      (ben�tigt f�r Blenden in mehreren Schritten)
 * \param iLength       Anzahl Samples die ab Beginn von pfData modifiziert werden.
 */
void ITA_BASE_API Fade( float* pfData, int iFadeLength, int iFadingSign, int iFadingFunction, int iOffset, int iLength );

//! Signale kreuzblenden
/**
 * F�hrt eine Kreuzblende von pfSrc1 nach pfSrc2 durch und speichert das Ergebnis in pfDest.
 * pfDest darf auch gleich pfSrc(1|2) sein.
 *
 * \param pfSrc1		Quellpuffer (von)
 * \param pfSrc2		Quellpuffer (nach)
 * \param pfDest		Zielpuffer (von)
 * \param iFadeLength	L�nge der Kreuzblende (darf L�ngen der drei Puffer nicht �berschreiten)
 * @param iFadingFunction	Fading function
 */
void ITA_BASE_API Crossfade( const float* pfSrc1, const float* pfSrc2, float* pfDest, int iFadeLength, int iFadingFunction );

#endif // INCLUDE_WATCHER_ITA_FADE
