/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_STOP_WATCH
#define INCLUDE_WATCHER_ITA_STOP_WATCH

// ITABase
#include <ITABase/ITAStatistics.h>
#include <ITABaseDefinitions.h>
#include <ITAClock.h>
#include <ITACriticalSection.h>
#include <algorithm>

//! Eine Klasse zum Messen von Zeitintervallen
/**
 * ISPL::Stopwatch ist eine Klasse, welche die Funktionalit�t des
 * Zeitmessens wie mit einer Stoppuhr realisiert. Dazu verwendet
 * die Klasse die High Performance Timer (HPT). Ferner bietet sie
 * die M�glichkeit den Durchschnittswert der gemessenen Zeiten zu
 * ermitteln, was bei Benchmarking-Anwendung interessant ist.
 *
 * \important Die Klasse ist nicht (mehr) Thread-Safe! Um eine hohe
 *            Messgenauigkeit zu erreichen, wurde auf
 *            Synchronisationsmechanismen verzichtet. Sie m�ssen
 *            in ihrem Code selbst daf�r Sorgen, das Zugriffe auf die
 *            Methoden von Instanzen der Klasse exklusiv geschehen!
 */

class ITA_BASE_API ITAStopWatch
{
public:
	//! Konstruktor
	ITAStopWatch( bool bStartImmediately = false );

	//! Konstruktor
	ITAStopWatch( ITAClock* pClock );

	//! Zur�cksetzen
	/**
	 * Setzt den internen Z�hler f�r die Anzahl der Messzyklen,
	 * sowie den Minimal-, Maximal- und Mittelwert zur�ck.
	 *
	 * \note Laufende Zeitnahmen werden abgebrochen
	 */
	void reset( );

	//! Feststellen ob die Zeitnahme bereits gestartet wurde
	bool started( ) const;

	//! Zeitnahme beginnen
	/**
	 * \note Wurde bereits die Zeitnahme gestartet, wird eine Ausnahme ausgel�st
	 */
	void start( );

	//! Zeitnahme stoppen
	/**
	 * Stoppt die aktuelle Zeitnahme und gibt die Dauer des gemessenen
	 * Zeitintervalls in Sekunden zur�ck.
	 *
	 * \return Gemessenes Zeitinterval [in Sekunden]
	 *
	 * \note Ist keine Zeitnahme gestartet, wird eine Ausnahme ausgel�st
	 */
	double stop( );

	//! Anzahl der gemessenen Zeitintervalle (Messzyklen) zur�ckgeben
	unsigned int cycles( ) const;

	//! Setzt Anzahl der ber�cksichtigten Zeitintervalle (Messzyklen)
	// Deprecated: void setMaxCycles(unsigned int uiMaxCycles);

	//! K�rzeste gemessene Zeit zur�ckgeben
	/**
	 * \return K�rzeste gemessene Zeit [in Sekunden]
	 * \note Wurden noch keine Messungen durchgef�hrt, ist der R�ckgabewert 0
	 */
	double minimum( ) const;

	//! L�ngste gemessene Zeit zur�ckgeben
	/**
	 * \return L�ngste gemessene Zeit [in Sekunden]
	 * \note Wurden noch keine Messungen durchgef�hrt, ist der R�ckgabewert 0
	 */
	double maximum( ) const;

	//! Summierte Laufzeiten seit dem letzten Reset aller gemessenen Zeiten zur�ckgeben
	/**
	 * \return Summe aller gemessenen Zeiten [in Sekunden]
	 * \note Wurden noch keine Messungen durchgef�hrt, ist der R�ckgabewert 0
	 */
	inline double sum( ) { return m_dSum; }

	//! Arithmetischer Mittelwert (mean) aller gemessenen Zeiten zur�ckgeben
	/**
	 * \return Arithmetischer Mittelwert (mean) aller gemessenen Zeiten [in Sekunden]
	 * \note Wurden noch keine Messungen durchgef�hrt, ist der R�ckgabewert 0
	 */
	double mean( ) const;

	//! Varianz (Moment 2. Ordnung) aller gemessenen Zeiten zur�ckgeben
	/**
	 * Die Varianz (das Moment 2. Ordnung) aller gemessenen Zeiten berechnet
	 * sich zu omega = sqrt( 1/n * sum_over_x[i](mju - x[i]) ), wobei
	 * mju der arithmethische Mittelwert (mean, oder auch Erwartungswert)
	 * und die x[i] die Messwerte sind.

	 * \return Varianz aller gemessenen Zeiten [in Sekunden]
	 * \note Wurden noch keine Messungen durchgef�hrt, ist der R�ckgabewert 0
	 */
	double variance( ) const;

	//! Standardabweichung aller gemessenen Zeiten zur�ckgeben
	/**
	 * Die Standardabweichung aller gemessenen Zeiten berechnet
	 * sich als Quadratwurzel der Varianz.
	 *
	 * \return Standardabweichung aller gemessenen Zeiten [in Sekunden]
	 * \note Wurden noch keine Messungen durchgef�hrt, ist der R�ckgabewert 0
	 */
	double std_deviation( ) const;

	//! Selbsttest durchf�hren
	/**
	 * Diese Methode misst den Durchschnittswert der Dauer zwischen
	 * zwei direkten aufeinanderfolgenden Aufrufen der Methoden
	 * start() und stop(). Idealerweise sollte dieser Wert nahe 0 sein.
	 * (Hinweis: Die Stopwatch ber�cksichtigt eine Grundlatenz (Verz�gerung
	 * zwischen Aufrufen von start() und stop()), welche von allen Messwerten
	 * abgezogen wird die stop() zur�ckgibt).
	 *
	 * In der Praxis ergeben sich gr��ere Werte, falls im Debug-Modus
	 * kompiliert wurde. In allen F�llen sollte der Wert aber <= 1ns sein.
	 * (Testsystem: Pentium M 1.4 GHz)
	 *
	 * \return Durchschnittsdauer in Sekunden
	 */
	static double selftest( );

	//! Einen formattierten String ausgeben, der den status der Stopwatch formuliert
	std::string ToString( ) const;

	//! Returns statistics as class
	ITABase::CStatistics GetStatistics( std::string sName ) const;

private:
	bool m_bStarted;
	double m_dStart, m_dStop;
	double m_dSum, m_dSquareSum, m_dMin, m_dMax;
	unsigned int m_uiCycles;

	ITAClock* m_pClock;
	static bool m_bInstanceCreated;    // Bereits eine Instanz dieser Klasse erzeugt?
	static double m_dStartStopLatency; // Latenz zwischen Start/Stop (Messfehler)
	ITACriticalSection m_csReadWrite;
};

//! Outstream operator
inline std::ostream& operator<<( std::ostream& os, ITAStopWatch& sw )
{
	os << sw.ToString( );
	return os;
};

#endif // INCLUDE_WATCHER_ITA_STOP_WATCH
