/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2022
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_TIME_SERIES_ANALYZER
#define INCLUDE_WATCHER_ITA_TIME_SERIES_ANALYZER

#include <ITABaseDefinitions.h>
#include <algorithm>
#include <cmath>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

//! Datenstruktur die Analysewerte f�r ein Zeitsignal speichert
template<typename T>
struct ITATimeseriesAnalyzerData
{
	T minimum;            // Minimalwert
	T maximum;            // Maximalwert
	double dMean;         // Arithmetischer Mittelwert (Erwartungswert)
	double dVariance;     // Varianz
	double dStdDeviation; // Standabweichung
};

/**
 * Ein Klassentemplate, welches Funktionalit�t definiert um ein Zeitsignal
 * zu analysieren. Einer Instanz des Klassentemplates werden Werte (samples)
 * des Templateparameter-Datentyps �bergeben. Die Instanz analysiert das
 * so entstehende Zeitsignal und bestimmt dessen Grenzen (Minimum, Maximum),
 * sowie statistische Merkmale (Erwartungswert, Varianz, Standardabweichung).
 */
template<typename T>
class ITATimeseriesAnalyzer
{
public:
	inline ITATimeseriesAnalyzer( ) { reset( ); };

	//! Zur�cksetzen
	/**
	 * Setzt alle Analysewerte zur�ck.
	 */
	inline void reset( )
	{
		m_minimum    = ( std::numeric_limits<T>::max )( );
		m_maximum    = ( std::numeric_limits<T>::min )( );
		m_uiSamples  = 0;
		m_dSum       = 0;
		m_dSquareSum = 0;
	};

	//! Einen Wert (sample) �bergeben
	inline void handle( const T& value )
	{
		m_minimum = ( std::min )( m_minimum, value );
		m_maximum = ( std::max )( m_maximum, value );
		double x  = (double)value;
		m_dSum += x;
		m_dSquareSum += ( x * x );
		m_uiSamples++;
	};

	//! Mehrere Werte (sample) �bergeben
	inline void handle( const T* value, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			handle( value[i] );
	};

	//! Mehrere Werte (sample) �bergeben
	inline void handle( const std::vector<T>& values )
	{
		for( size_t i = 0; i < values.size( ); ++i )
			handle( values[i] );
	};

	//! Anzahl der �bergebenen Werte zur�ck
	inline unsigned int samples( ) const { return m_uiSamples; };

	//! Kleinsten Wert zur�ckgeben
	/**
	 * \note Wurden noch keine Werte zur Analyse �bergeben, ist der R�ckgabewert 0
	 */
	inline T minimum( ) const { return ( m_uiSamples == 0 ? 0 : m_minimum ); };

	//! Gr��ten Wert zur�ckgeben
	/**
	 * \note Wurden noch keine Werte zur Analyse �bergeben, ist der R�ckgabewert 0
	 */
	inline T maximum( ) const { return ( m_uiSamples == 0 ? 0 : m_maximum ); };

	//! Arithmetischer Mittelwert (mean) zur�ckgeben
	/**
	 * \return Arithmetischer Mittelwert (mean) aller �bergebenen Werte
	 * \note Wurden noch keine Werte zur Analyse �bergeben, ist der R�ckgabewert 0
	 */
	inline double mean( ) const { return ( m_uiSamples == 0 ? 0 : m_dSum / (double)m_uiSamples ); };

	//! Varianz (Moment 2. Ordnung) zur�ckgeben
	/**
	 * Die Varianz (das Moment 2. Ordnung) aller �bergebenen Werte berechnet
	 * sich zu omega = sqrt( 1/n * sum_over_x[i](mju - x[i]) ), wobei
	 * mju der arithmethische Mittelwert (mean, oder auch Erwartungswert)
	 * und die x[i] die bisher aufgenommenen Werte sind.
	 *
	 * \return Varianz
	 * \note Wurden noch keine Werte zur Analyse �bergeben, ist der R�ckgabewert 0
	 */
	inline double variance( ) const
	{
		if( m_uiSamples == 0 )
			return 0;
		// Formel: sigma = E(X^2) - E(X)^2
		double x = m_dSum / (double)m_uiSamples;
		return ( m_uiSamples == 0 ? 0 : m_dSquareSum / (double)m_uiSamples - x * x );
	};

	//! Standardabweichung aller aufgenommenen Werte zur�ckgeben
	/**
	 * Die Standardabweichung aller aufgenommenen Werte berechnet
	 * sich als Quadratwurzel der Varianz.
	 *
	 * \return Standardabweichung
	 * \note Wurden noch keine Werte zur Analyse �bergeben, ist der R�ckgabewert 0
	 */
	inline double std_deviation( ) const { return sqrt( variance( ) ); };

	//! Alle Informationen als 5-Tupel zur�ckgeben
	inline ITATimeseriesAnalyzerData<T> getData( )
	{
		ITATimeseriesAnalyzerData<T> data;
		data.minimum       = minimum( );
		data.maximum       = maximum( );
		data.dMean         = mean( );
		data.dVariance     = variance( );
		data.dStdDeviation = std_deviation( );
		return data;
	};

	//! Informationen �ber den Analysator als Zeichenkette zur�ckgeben
	inline std::string toString( )
	{
		std::stringstream ss;
		ss << "ITATimeseriesAnalyzer { samples: " << m_uiSamples << ", min: " << minimum( ) << ", max: " << maximum( ) << ", mean: " << mean( )
		   << ", var: " << variance( ) << ", stdder: " << std_deviation( ) << " }" << std::endl;
		return ss.str( );
	};

private:
	unsigned int m_uiSamples; // Anzahl �bergebener Werte
	T m_minimum, m_maximum;
	double m_dSum;       // Summe aller Messwerte
	double m_dSquareSum; // Summe der Messwertquadrate f�r Berechnung der Varianz und StdDer.
};

#endif // INCLUDE_WATCHER_ITA_TIME_SERIES_ANALYZER
