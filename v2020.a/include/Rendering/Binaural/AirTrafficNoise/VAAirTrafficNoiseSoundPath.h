/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2020
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_BINAURAL_AIR_TRAFFIC_SOUND_PATH
#define IW_VACORE_BINAURAL_AIR_TRAFFIC_SOUND_PATH

#include "VAAirTrafficNoiseSoundSource.h"
#include "VAAirTrafficNoiseSoundReceiver.h"

// VA includes
#include <VA.h>
#include <VAObjectPool.h>

#include "../../../Motion/VAMotionModelBase.h"
#include "../../../Motion/VASharedMotionModel.h"
#include "../../../Scene/VAScene.h"
#include "../../../VASourceTargetMetrics.h"
#include "../../../core/core.h"
#include "../../../Filtering/VATemporalVariations.h"
#include "../../../directivities/VADirectivityDAFFEnergetic.h"
#include "../../../directivities/VADirectivityDAFFHRIR.h"

// ITA includes
#include <ITADataSourceRealization.h>
#include <ITASampleBuffer.h>
#include <ITAVariableDelayLine.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <ITAUPConvolution.h>
#include <ITAUPFilter.h>
#include <ITAUPFilterPool.h>
#include <ITAThirdOctaveFilterbank.h>

// 3rdParty includes
#include <tbb/concurrent_queue.h>

// STL Includes

class CVABATNSoundPath : public CVAPoolObject
{
public:
	virtual ~CVABATNSoundPath();

	//! Retarded metrics of sound path
	class Metrics
	{
	public:
		double dRetardedDistance;		  //!< Metrical distance to retarded sound position
		VAQuat qAngleRetSourceToListener; //!< Immediate angle of incidence to retarded source position in listener reference frame in YPR convention
		VAQuat qAngleListenerToRetSource; //!< Retarded angle of incidence to listener in source reference frame in YPR convention
	};

	//! State of directivity
	class CSoundSourceDirectivityState
	{
	public:
		inline CSoundSourceDirectivityState()
			: pData( NULL )
			, iRecord( -1 )
			, bDirectivityEnabled( true ) {};

		IVADirectivity* pData;	//!< Directivity data, may be NULL
		int iRecord;			//!< Directivity index
		bool bDirectivityEnabled;

		inline bool operator==( const CSoundSourceDirectivityState& rhs ) const
		{
			bool bBothEnabled = ( bDirectivityEnabled == rhs.bDirectivityEnabled );
			bool bSameRecordIndex = ( iRecord == rhs.iRecord );
			bool bSameData = ( pData == rhs.pData );

			return ( bBothEnabled && bSameRecordIndex && bSameData );
		};
	};

	class CSoundReceiverDirectivityState
	{
	public:
		inline CSoundReceiverDirectivityState()
			: pData( NULL )
			, iRecord( -1 )
			, fDistance( 1.0f ) {};

		IVADirectivity* pData;	 //!< HRIR data, may be NULL
		int iRecord;			 //!< HRIR index
		float fDistance;		 //!< HRIR dataset distance

		inline bool operator!=( const CSoundReceiverDirectivityState& rhs ) const
		{
			if( pData != rhs.pData ) return true;
			if( fDistance != rhs.fDistance ) return true;
			if( iRecord != rhs.iRecord ) return true;
			return false;
		};
	};

	CVABATNSoundSource* pSoundSource;		//!< Verkn�pfte Quelle
	CVABATNSoundReceiver* pSoundReceiver;	//!< Verkn�pfter H�rer

	struct CPropPath
	{
		CVASourceTargetMetrics oRelations; //!< Informationen �ber die Relation von Quelle und H�rer (Position & Orientierung)

		double dPropagationTime; //!< Time that the sound waves took to be propagated to the receiver
		double dGeometricalSpreadingLoss; //!< Spherical / geometrical spreading loss (usually 1-by-R distance law)

		CITAVariableDelayLine* pVariableDelayLine;	//!< DSP-Element zur Ber�cksichtigung der Mediumsausbreitung in instation�ren Schallfeldern (inkl. Doppler-Effekt, falls aktiviert)


		CSoundSourceDirectivityState oDirectivityStateCur;	//!< Aktueller Status der Richtcharakteristik
		CSoundSourceDirectivityState oDirectivityStateNew;	//!< Neuer Status der Richtcharakteristik
		ITABase::CThirdOctaveGainMagnitudeSpectrum oSoundSourceDirectivityMagnitudes; //!< Magnitudes for directivity

		CVAAtmosphericTemporalVariantions oATVGenerator;	//!< Generator for temporal variation magnitudes of the time signal by medium shifts during propagation
		ITABase::CThirdOctaveGainMagnitudeSpectrum oTemporalVariationMagnitudes;		//!< Magnitudes for temporal variation of the time signal by medium shifts during propagation

		ITABase::CThirdOctaveGainMagnitudeSpectrum oAirAttenuationMagnitudes; //!< Magnitudes for air attenuation (damping)

		ITABase::CThirdOctaveGainMagnitudeSpectrum oGroundReflectionMagnitudes; //!< Ground reflection parameters (only for reflection paths)

		CITAThirdOctaveFilterbank* pThirdOctaveFilterBank; //!< DSP-Element zur Filterung von Third-Octave Datens�tzen der Richtcharakteristik
		ITABase::CThirdOctaveGainMagnitudeSpectrum oThirdOctaveFilterMagnitudes; //!< DSP magnitudes (third octave band resolution)

		CSoundReceiverDirectivityState oSoundReceiverDirectivityStateCur;	//!< Aktueller Status der HRIR
		CSoundReceiverDirectivityState oSoundReceiverDirectivityStateNew;	//!< Neuer Status der HRIR

		ITAUPConvolution* pFIRConvolverChL;	//!< DSP-Element zur Faltung mit der Kopfbezogenen �bertragungsfunktion im Zeitbereich (HRIR) - Links
		ITAUPConvolution* pFIRConvolverChR;	//!< DSP-Element zur Faltung mit der Kopfbezogenen �bertragungsfunktion im Zeitbereich (HRIR) - Rechts

		inline CPropPath()
			: dPropagationTime( 0.0f )
			, dGeometricalSpreadingLoss( 1.0f )
			, pVariableDelayLine( NULL )
			, pThirdOctaveFilterBank( NULL )
			, pFIRConvolverChL( NULL )
			, pFIRConvolverChR( NULL )
		{
			oGroundReflectionMagnitudes.SetIdentity();
		};
	};

	CPropPath oDirSoundPath; //!< Direct sound propagation path
	CPropPath oRefSoundPath; //!< Propagation path including ground reflection

	double dGroundReflectionPlanePosition; //!< Height of ground, defaults to 0

	std::atomic< bool > bDelete; //!< Schallpfad zur L�schung markiert?

	inline void PreRequest()
	{
		pSoundSource = nullptr;
		pSoundReceiver = nullptr;

		// Reset DSP elements
		oDirSoundPath.pThirdOctaveFilterBank->Clear();
		oRefSoundPath.pThirdOctaveFilterBank->Clear();
		oDirSoundPath.pVariableDelayLine->Clear();
		oRefSoundPath.pVariableDelayLine->Clear();
		oDirSoundPath.pFIRConvolverChL->clear();
		oRefSoundPath.pFIRConvolverChL->clear();
		oDirSoundPath.pFIRConvolverChR->clear();
		oRefSoundPath.pFIRConvolverChR->clear();
	};


	//! Bestimmt die relativen Gr��en des Pfades
	/**
	* Diese berechneten Gr��en dienen als Grundlage zur Bestimmung der ausgew�hlten
	* Datens�tze und Einstellungen der DSP-Elemente. Ein weiteres Update der einzelnen
	* DSP-Elemente f�hrt z.B. zum Filteraustausch, wenn die Status�nderung Auswirkungen hat
	* (tats�chlich ein neuer Datensatz geholt werden muss).
	*
	* Diese Methode ist besonders leichtgewichtig, da sie im StreamProcess genutzt wird.
	*
	* // sp�ter -> \return Gibt false zur�ck, falls die retardierten Werte noch nicht zur Verf�gung stehen.
	*/
	void UpdateMetrics();

	//! Aktualisiert die einzelnen Komponenten des Schallpfades
	void UpdateEntities( int iEffectiveAuralisationMode );

	//! Aktualisiert die Richtcharakteristik auf dem Pfad
	void UpdateSoundSourceDirectivity();

	//! Aktualisiert die Mediumsausbreitung auf dem Pfad
	void UpdateMediumPropagation( double dSpeedOfSound );

	//! Updates the temporal variations model of medium shift fluctuation
	void UpdateTemporalVariation();

	//! Updates the air attenuation (damping)
	void UpdateAirAttenuation( const double dTemperatur = 20.0f, const double  dPressure = 101.325e3, const double dHumidity = 60.0f );

	//! Aktualisiert den Lautst�rkeabfall nach dem 1/r-Gesetzt (Inverse Distance Decrease/Law)
	void CalculateInverseDistanceDecrease();

	//! Aktualisiert die HRIR auf dem Pfad
	void UpdateSoundReceiverDirectivity();

	//! Aktiviert/Deaktiviert Doppler-Verschiebung
	void SetDopplerShiftsEnabled( bool bEnabled );

private:

	//! Standard-Konstruktor deaktivieren
	CVABATNSoundPath();

	//! Konstruktor
	CVABATNSoundPath( double dSamplerate, int iBlocklength, int iHRIRFilterLength, int iDirFilterLength, int iFilterBankType = CITAThirdOctaveFilterbank::IIR_BIQUADS_ORDER10 );

	ITASampleFrame m_sfHRIRTemp;		 //!< Intern verwendeter Zwischenspeicher f�r HRIR Daten�tze
	int m_iDefaultVDLSwitchingAlgorithm; //!< Umsetzung der Verz�gerungs�nderung


	friend class CVABATNSoundPathFactory;
};


// Factory

class CVABATNSoundPathFactory : public IVAPoolObjectFactory
{
public:

	inline CVABATNSoundPathFactory( double dSamplerate, int iBlocklength, int iHRIRFilterLength, int iDirFilterLength, int iFilterBankType = CITAThirdOctaveFilterbank::IIR_BIQUADS_ORDER10 )
		: m_dSamplerate( dSamplerate )
		, m_iBlocklength( iBlocklength )
		, m_iHRIRFilterLength( iHRIRFilterLength )
		, m_iDirFilterLength( iDirFilterLength )
		, m_iFilterBankType( iFilterBankType )
	{};

	inline CVAPoolObject* CreatePoolObject()
	{
		return new CVABATNSoundPath( m_dSamplerate, m_iBlocklength, m_iHRIRFilterLength, m_iDirFilterLength, m_iFilterBankType );
	};

private:
	double m_dSamplerate;		//!< Abtastrate
	int m_iBlocklength;			//!< Blockl�nge
	int m_iHRIRFilterLength;	//!< Filterl�nge der HRIR
	int m_iDirFilterLength;		//!< Filterl�nge der Richtcharakteristik
	int m_iFilterBankType;		//!< Filter bank type (FIR, IIR)
};

#endif // IW_VACORE_BINAURAL_AIR_TRAFFIC_SOUND_PATH
