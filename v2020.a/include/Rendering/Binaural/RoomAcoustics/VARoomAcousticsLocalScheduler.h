/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2020
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_ROOMACOUSTICS_LOCAL_SCHEDULER
#define IW_VACORE_ROOMACOUSTICS_LOCAL_SCHEDULER

#if (VACORE_WITH_RENDERER_BINAURAL_ROOM_ACOUSTICS==1)

#include <ITASimulationScheduler/LocalScheduler.h>

// ITA includes
#include <ITASampleFrame.h>

// Vista includes
#include <VistaInterProcComm/Concurrency/VistaThreadEvent.h>
#include <VistaInterProcComm/Concurrency/VistaThreadLoop.h>
#include <VistaInterProcComm/Concurrency/VistaPriority.h>

// 3rdParty includes
#include <tbb/concurrent_queue.h>

// STL includes
#include <atomic>
#include <cassert>
#include <list>

// VA includes
#include <VAObject.h>

// Forward declarations
class CVACoreImpl;

class CVARavenLocalScheduler : public ITASimulationScheduler::CLocalScheduler, public CVAObject {
public:
	CVARavenLocalScheduler(CVACoreImpl*, const ITASimulationScheduler::CLocalScheduler::CConfiguration& );
	virtual ~CVARavenLocalScheduler() {};

	//! Call-Hook f�r Objekt-Aufruf
	CVAStruct CallObject( const CVAStruct& oArgs );

	//! Informationsr�ckgabe VAObject
	CVAObjectInfo GetObjectInfo() const;
	
private:
	CVACoreImpl* m_pCore; //!< Zeiger auf erzeugenden VACore
};

#endif // (VACORE_WITH_RENDERER_BINAURAL_ROOM_ACOUSTICS==1)

#endif // IW_VACORE_ROOMACOUSTICS_LOCAL_SCHEDULER
