/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2020
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_BINAURAL_OUTDOOR_NOISE_AUDIO_RENDERER
#define IW_VACORE_BINAURAL_OUTDOOR_NOISE_AUDIO_RENDERER

#if VACORE_WITH_RENDERER_BINAURAL_OUTDOOR_NOISE

// VA Includes
#include <VA.h>
#include "../../VAAudioRenderer.h"
#include <ITAThirdOctaveFilterbank.h>



// ITA includes
#include <ITADataSourceRealization.h>

// Other
#include "../Clustering/Engine/VABinauralClusteringEngine.h"
#include "../Clustering/VABinauralWaveFront.h"
#include "../Clustering/Receiver/VABinauralClusteringReceiver.h"
#include "VABinauralOutdoorWaveFront.h"


class CVABinauralOutdoorWaveFront;
class CVABinauralOutdoorSource;

//! A binaural outdoor noise renderer that handles propagation paths efficiently
/**
  *  
  *
  */
class CVABinauralOutdoorNoiseRenderer : public IVAAudioRenderer, public ITADatasourceRealization
{
public:
	CVABinauralOutdoorNoiseRenderer( const CVAAudioRendererInitParams& );
	~CVABinauralOutdoorNoiseRenderer();

	inline void LoadScene( const std::string& ) {};

	void ProcessStream( const ITAStreamInfo* pStreamInfo );

	//! Handles scene updates for clustering stages
	void UpdateScene( CVASceneState* pNewSceneState );

	void Reset();

	void UpdateGlobalAuralizationMode( int iGlobalAuralizationMode );

	ITADatasource* GetOutputDatasource();

	void SetParameters( const CVAStruct& );
	CVAStruct GetParameters( const CVAStruct& ) const;

private:
	const CVAAudioRendererInitParams m_oParams;

	std::map< int, CVABinauralOutdoorSource* > m_mSources;
	std::map< int, CVABinauralClusteringReceiver* > m_mBinauralReceivers;
	std::map< int, CVABinauralOutdoorWaveFront* > m_mPaths; // All path of entire scene for all sources and receivers
	std::map< std::string, int > m_mHashIdentifier2PathID;
	int m_iLastPathID; // Used to assign unique wave front IDs
	//map containing all the paths, each accessed with a unique string key

	CVABinauralClusteringEngine* m_pClusterEngine;

	CVACoreImpl* m_pCore;

	CVASceneState* m_pNewSceneState;
	CVASceneState* m_pCurSceneState;

	IVAObjectPool* m_pWaveFrontPool;
	IVAObjectPool* m_pReceiverPool;

	std::atomic< bool > m_bIndicateReset, m_bResetAck;

	CVABinauralClusteringReceiver::Config m_oDefaultReceiverConf; //!< Default listener config for factory object creation
	CVABinauralOutdoorWaveFront::Config m_oDefaultWaveFrontConf;

	int m_iMaxDelaySamples;
	int m_iDefaultVDLSwitchingAlgorithm;
	int m_iHRIRFilterLength;
	int m_iCurGlobalAuralizationMode; // @todo use in UpdateScene

	void Init( const CVAStruct& oArgs );

	void UpdateSoundReceivers( CVASceneStateDiff* diff );
	void UpdateSoundSources( CVASceneStateDiff* diff );

	void CreateSoundReceiver( const int iID, const CVAReceiverState* pState );
	void CreateSoundSource( const int iID, const CVASoundSourceState* pState );

	void DeleteSoundReceiver( const int iID );
	void DeleteSoundSource( const int iiD );

	void UpdateMotionStates();
	void UpdateTrajectories( double dTime );

	double m_dAngularDistanceThreshold;
	int m_iNumClusters;

#ifdef BINAURAL_OUTDOOR_NOISE_INSOURCE_BENCHMARKS
	ITAStopWatch m_swProcessStream;
#endif
};

#endif // VACORE_WITH_RENDERER_BINAURAL_OUTDOOR_NOISE

#endif // IW_VACORE_BINAURAL_OUTDOOR_NOISE_AUDIO_RENDERER
