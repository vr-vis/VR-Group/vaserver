/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2020
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VACORE_BINAURAL_OUTDOOR_SOURCE
#define IW_VACORE_BINAURAL_OUTDOOR_SOURCE

#include <ITASIMOVariableDelayLineBase.h>

#include "../../../Motion/VASharedMotionModel.h"
#include "../../../Scene/VASoundSourceState.h"
#include "../../../Scene/VASoundSourceDesc.h"

//! 
/**
  * @henry: 
  */
class CVABinauralOutdoorSource
{
public:
	CVABinauralOutdoorSource( int iMaxDelaySamples );
	~CVABinauralOutdoorSource();

	CVASoundSourceState* pState;
	CVASoundSourceDesc* pData;
	CVASharedMotionModel* pMotionModel;

	CITASIMOVariableDelayLineBase* pVDL;
	
private:
};

#endif // IW_VACORE_BINAURAL_OUTDOOR_SOURCE
